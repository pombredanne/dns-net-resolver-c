using System;

namespace Heijden.DNS
{
	public class RecordUnknown : Record
	{
		public RecordUnknown(RecordReader rr)
		{
			rr.Position -=2;
			// re-read length
			ushort RDLENGTH = rr.ReadShort();
			// skip bytes
			rr.ReadBytes(RDLENGTH);
		}
	}
}
