using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace DnsDig
{
	class FeedbackWriter : TextWriter
	{
		internal FeedbackWriter(TextBox textBox)
			: base()
		{
			_textBox = textBox;
		}

		private TextBox _textBox;


		public override Encoding Encoding
		{
			get { return Encoding.Default; }
		}

		private delegate void WriteDelegate(string value);
		public override void Write(string value)
		{
			if (_textBox.InvokeRequired)
			{
				_textBox.Invoke(new WriteDelegate(Write), new object[] { value });
			}
			else
			{
				_textBox.AppendText(value.Replace("\n", base.NewLine));
			}
		}

		public override void WriteLine(string value)
		{
			this.Write(value);
			this.Write(base.NewLine);
		}
	}
}
